/*
 * TCompetition.cpp
 *
 *  Created on: Oct 19, 2020
 *      Author: phaentu
 */

#include "TCompetition.h"


//----------------------------------
// TPopSizeTrajectory
//----------------------------------
TPopSizeTrajectory::TPopSizeTrajectory(const uint32_t duration, const uint32_t N0){
	reset(duration, N0);
};

void TPopSizeTrajectory::reset(const uint32_t duration, const uint32_t N0){
	_N.resize(duration + 1);
	std::fill(_N.begin(), _N.end(), 0);
	_N[0] = N0;
};

TPopSizeTrajectories::TPopSizeTrajectories(const uint32_t duration, const std::vector<uint32_t> N0){
	reset(duration, N0);
};

void TPopSizeTrajectories::reset(const uint32_t duration, const std::vector<uint32_t> N0){
	_Ns.resize(N0.size());
	for(size_t i=0; i<N0.size(); ++i){
		_Ns[i].reset(duration, N0[i]);
	}
};

//----------------------------------
// TPopSizeProbabilityDist
//----------------------------------
TPopSizeProbabilityDist::TPopSizeProbabilityDist(){
	reset(0);
};

TPopSizeProbabilityDist::TPopSizeProbabilityDist(const uint32_t initialN){
	reset(initialN);
};

void TPopSizeProbabilityDist::reset(const uint32_t initialN){
	_minNStorage = initialN - EXPANDSIZE;
	_maxNStorage = initialN + EXPANDSIZE;
	if(_minNStorage < 0){
		_minNStorage = 0;
	}
	_probs.resize(_maxNStorage - _minNStorage + 1);
	std::fill(_probs.begin(), _probs.end(), 0);
	_probs[initialN - _minNStorage] = 1.0;

	//set relevant range. Make sure _relevantMinN > 0
	if(initialN == 0){
		_minN = 1;
	} else {
		_minN = initialN;
	}
	_maxN = initialN;
};

void TPopSizeProbabilityDist::_expand(){
	if(_minNStorage >= _minN){
		uint32_t diff = EXPANDSIZE;
		if(_minNStorage < diff){
			diff = _minNStorage;
		}
		_probs.insert(_probs.begin(), diff, 0.0);
		_minNStorage -= diff;
	}

	 if(_maxNStorage <= _maxN){
		 _maxNStorage = _maxNStorage + EXPANDSIZE;
		 _probs.insert(_probs.end(), EXPANDSIZE, 0.0);
	 }
};

void TPopSizeProbabilityDist::writeQuantiles(TOutputFile & out, const std::vector<double> & quantiles) const{
	double sum = 0.0;
	size_t nextQuantile = 0;
	for(uint32_t N = _minN; N <= _maxN; ++N){
		sum += _probs[N - _minNStorage];
		if(sum >= quantiles[nextQuantile]){
			if(sum > quantiles[nextQuantile]){
				out << N - 0.5;
			} else if(sum == quantiles[nextQuantile]){
				out << N;
			}
			++nextQuantile;
			if(nextQuantile == quantiles.size()){
				break;
			}
		}
	}

	//write last, if necessary
	while(nextQuantile < quantiles.size()){
		out << _maxN;
		++nextQuantile;
	}
};

double TPopSizeProbabilityDist::calculateR(const uint32_t obsN) const{
	double R = 0.0;
	for(uint32_t N = _minN; N <= _maxN; ++N){
		R += obsN / (double) (N) * _probs[N - _minNStorage];
	}

	return R;
};

void TPopSizeProbabilityDist::print() const{
	std::cout << "TPopSizeProbabilityDist:" << std::endl;
	double sum = 0.0;
	for(uint32_t N = _minN; N <= _maxN; ++N){
		std::cout << N << "\t" << _probs[N - _minNStorage] << std::endl;
		sum += _probs[N - _minNStorage];
	}
	std::cout << "sum\t" << sum << std::endl;
};

TPopSizeProbabilityDistKolmogorov::TPopSizeProbabilityDistKolmogorov(){
	reset(0);
};

TPopSizeProbabilityDistKolmogorov::TPopSizeProbabilityDistKolmogorov(const uint32_t initialN){
	reset(initialN);
};

void TPopSizeProbabilityDistKolmogorov::_resetDerivatives(){
	_derivatives.resize(_maxNStorage + 1);
};

void TPopSizeProbabilityDistKolmogorov::reset(const uint32_t N0){
	TPopSizeProbabilityDist::reset(N0);
	_resetDerivatives();
};

void TPopSizeProbabilityDistKolmogorov::_expand(){
	TPopSizeProbabilityDist::_expand();
	_derivatives.resize(_probs.size());
};

void TPopSizeProbabilityDistKolmogorov::kolmogorovStepOneSpecies(const double growthRate, const double competition, const double deltaT){
	//make one Kolmorov step
	//ensure size
	_expand();

	//calculate derivatives
	uint32_t index = _minN - _minNStorage;
	double previousBirth = 0;
	double nextDeath = competition * _minN * _minN * _probs[index];
	_derivatives[index - 1] = nextDeath;

	for(uint32_t N = _minN; N <= _maxN; ++N, ++index){
		//update
		double birth = growthRate * N * _probs[index];

		//update death
		double death = nextDeath;
		uint32_t nextN = N + 1;
		nextDeath = competition * nextN * nextN * _probs[index + 1];

		//calculate derivative and update p
		_derivatives[index] = previousBirth + nextDeath - birth - death;

		//update previous birth
		previousBirth = birth;
	}

	//calculate last derivative
	_derivatives[index] = previousBirth;

	//update p
	--_minN;
	++_maxN;
	for(index = _minN - _minNStorage; index <= _maxN - _minNStorage; ++index){
		_probs[index] += _derivatives[index] * deltaT;
	}

	//only expand range if value is meaningful
	if(_probs[_minN - _minNStorage] < MINPROBCONSIDERED){
		++_minN;
	}
	if(_probs[_maxN - _minNStorage] < MINPROBCONSIDERED){
		--_maxN;
	}

	//make sure _relevantMinN is > 0
	if(_minN == 0){
		_minN = 1;
	}
};


TPopSizeProbabilityDistSequence::TPopSizeProbabilityDistSequence(const uint32_t duration, const uint32_t N0){
	reset(duration, N0);
};

void TPopSizeProbabilityDistSequence::reset(const uint32_t duration, const uint32_t N0){
	_dists.resize(duration + 1);
	_dists[0].reset(N0);
};

void TPopSizeProbabilityDistSequence::writeQuantiles(const std::string filename, const std::vector<double> & quantiles, const uint32_t addToTime) const{
	//open output file
	std::vector<std::string> header = {"time"};
	for(const auto& q : quantiles){
		header.push_back(toString(q));
	}
	TOutputFile out(filename, header);

	//write quantiles
	for(size_t t = 0; t<_dists.size(); ++t){
		out << t + addToTime;
		_dists[t].writeQuantiles(out, quantiles);
		out << std::endl;

	}
};

//----------------------------------
// TPopSizeJointStateProbabilities
//----------------------------------
TPopsizeJointStateProbabilities::TPopsizeJointStateProbabilities(){
	_firstN = 0;
};

TPopsizeJointStateProbabilities::TPopsizeJointStateProbabilities(TPopSizeProbabilityDist & p){
	reset(p);
};

void TPopsizeJointStateProbabilities::reset(TPopSizeProbabilityDist & p){
	_firstN = p.minN();
	_probs = std::vector<double>(p.begin(), p.end());
};



//----------------------------------
// TCompeition
//----------------------------------
TCompetition::TCompetition(TParameters & Parameters, TLog* Logfile){
	_logfile = Logfile;

	//-----------------------------------
	//read model parameters
	//-----------------------------------
	_logfile->startIndent("Reading competition model:");
	_numSpecies = 2; //currently only implemented for two species.

	//initial sizes
	Parameters.fillParameterIntoVector("initialSizes", _initialSizes, ',', "Provide comma-separated list of per-species initial population sizes.");
	if(_initialSizes.size() != _numSpecies)
		throw "Wrong number of growth rates: expected " + toString(_numSpecies) + " but found " + toString(_initialSizes.size()) + "!";
	_logfile->list("Will consider ", _numSpecies, " species.");
	_logfile->list("Initial population sizes: ", concatenateString(_initialSizes, ", "), " (parameter 'initialSizes')");

	//growth rates
	Parameters.fillParameterIntoVector("growthRates", _growthRates, ',', "Provide comma-separated list of per-species growth rates.");
	if(_growthRates.size() != _numSpecies)
		throw "Wrong number of growth rates: expected " + toString(_numSpecies) + " but found " + toString(_growthRates.size()) + "!";
	_logfile->list("Growth rates: ", concatenateString(_growthRates, ", "), " (parameter '_growthRates')");

	Parameters.fillParameterIntoVector("growthRateChange", _growthRateChange, ',', "Provide comma-separated list of per-species rates at which the growth rates change.");
		if(_growthRateChange.size() != _numSpecies)
			throw "Wrong number of growth rate change rates: expected " + toString(_numSpecies) + " but found " + toString(_growthRateChange.size()) + "!";
	_logfile->list("Rates of growth rate change: ", concatenateString(_growthRateChange, ", "), " (parameter 'growthRateChange')");

	//competition matrix
	std::string explanation = "Provide competition matrix as 'a_11,a12;a21,a22'.";
	std::vector<std::string> tmp;
	std::vector<double> tmpDouble;
	Parameters.fillParameterIntoVector("competitionMatrix", tmp, ';', explanation);
	if(tmp.size() != _numSpecies)
		throw "Wrong number of rows in competition matrix: expected " + toString(_numSpecies) + " but found " + toString(tmp.size()) + "!\n" + explanation;
	_competitionMatrix.resize(_numSpecies*2);
	std::string reportMatrix;
	for(int i=0; i<_numSpecies; ++i){
		fillVectorFromString(tmp[i], tmpDouble, ',');
		if(tmpDouble.size() != _numSpecies)
			throw "Wrong number of columns in competition matrix on row " + toString(i+1) + ": expected " + toString(_numSpecies) + " but found " + toString(tmpDouble.size()) + "!\n" + explanation;

		for(int j=0; j < _numSpecies; ++j){
			_competitionMatrix[i*_numSpecies + j] = tmpDouble[j];
		}

		//compile report string
		if(i>0) reportMatrix += ';';
		reportMatrix += concatenateString(tmpDouble, ",");
	}
	_logfile->list("competition matrix: ", reportMatrix, " (parameter 'competitionMatrix')");
	_logfile->endIndent();

	//read simulation parameters
	_logfile->startIndent("Simulation parameters:");

	//duration
	Parameters.fillParameterIntoVector("duration", _duration, ',', "Provide a comma-separated list of durations.");
	if(tmp.size() != _numSpecies)
		throw "Wrong number of durations: expected " + toString(_numSpecies) + " but found " + toString(_duration.size());
	_logfile->list("Will introduce species 2 after T_1 = " + toString(_duration[0]) + " and then simulate for additional T_2 = " + toString(_duration[1]) + " (T = " + toString(_duration[0] + _duration[1]) + "). (parameter 'duration')");

	//deltaT
	_stepsPerUnit = Parameters.getParameterIntWithDefault("stepsPerUnit", 1000);
	_deltaT = 1.0 / (double) _stepsPerUnit;
	_logfile->list("Will solve differential equations using " + toString(_stepsPerUnit) + " steps per unit of time (deltaT = " + toString(_deltaT) + "). (parameter 'stepsPerUnit')");

	//joint prob window
	_rhoWindow = Parameters.getParameterIntWithDefault("rhoWindow", 1);
	_logfile->list("Will calculate rho for " + toString(_rhoWindow) + " units of time. (parameter 'rhoWindow')");

	_logfile->endIndent();

	//out name
	_outName = Parameters.getParameterStringWithDefault("out", "temporalImpacts_");
};

void TCompetition::_simulateOneSpecies(TPopSizeTrajectory & N, const uint32_t duration, const uint32_t N0, const double growthRate, const double growthRateChange, const double competition, TRandomGenerator & RandomGenerator){
	//prepare storage for N
	N.reset(duration, N0);

	//simulate
	std::vector<double> transProb(2);
	for(uint32_t t=0; t<duration; ++t){
		uint32_t tmpN = N[t];

		//loop over deltaT steps
		for(uint32_t s=0; s<_stepsPerUnit; ++s){
			//get relevant growth rate
			double realTime = t + s * _deltaT;
			double growth = growthRate * exp(growthRateChange * realTime);

			//transition probabilities
			transProb[0] = growth * tmpN * _deltaT;
			transProb[1] = transProb[0] + tmpN * tmpN * competition * _deltaT;

			//check if deltaT is small enough
			if(transProb[1] > 1){
				throw "Transition probabilities > 1! Use more steps per time unit (parameter 'stepsPerUnit').";
			}

			double rand = RandomGenerator.getRand();
			if(rand < transProb[0]){
				++tmpN;
			} else if(rand < transProb[1]){
				--tmpN;
			}
		}

		//update N
		N[t+1] = tmpN;
		if(N[t+1] == 0){
			break;
		}
	}
};

void TCompetition::_simulateTwoSpecies(TPopSizeTrajectories & N, const uint32_t duration, std::vector<uint32_t> & N0, const std::vector<double> growthRates, const std::vector<double> & growthRateChange, const std::vector<double> competition, TRandomGenerator & RandomGenerator){
	//prepare storage for N
	N.reset(duration, N0);

	//simulate with two species
	std::vector<double> transProb(4);
	for(uint32_t t=0; t<duration; ++t){
		uint32_t tmpN0 = N[0][t];
		uint32_t tmpN1 = N[1][t];

		//loop over deltaT steps
		for(uint32_t s=0; s<_stepsPerUnit; ++s){
			//get relevant growth rate
			double realTime = t + s * _deltaT;
			double growth0 = growthRates[0] * exp(growthRateChange[0] * realTime);
			double growth1 = growthRates[1] * exp(growthRateChange[1] * realTime);

			//transition probabilities
			transProb[0] = growth0 * tmpN0 * _deltaT;
			transProb[1] = transProb[0] + growth1 * tmpN1 * _deltaT;
			transProb[2] = transProb[1] + tmpN0 * (competition[0] * tmpN0 + competition[1] * tmpN1) * _deltaT;
			transProb[3] = transProb[2] + tmpN1 * (competition[2] * tmpN0 + competition[3] * tmpN1) * _deltaT;

			//check if deltaT is small enough
			if(transProb[3] > 1){
				throw "Transition probabilities > 1! Use more steps per time unit (parameter 'stepsPerUnit').";
			}

			double rand = RandomGenerator.getRand();
			if(rand < transProb[0]){
				++tmpN0;
			} else if(rand < transProb[1]){
				++tmpN1;
			} else if(rand < transProb[2]){
				--tmpN0;
			} else if(rand < transProb[3]){
				--tmpN1;
			}
		}

		//update N
		uint32_t next = t + 1;
		N[0][next] = tmpN0;
		N[1][next] = tmpN1;
		if(N[0][next] == 0 && N[1][next] == 0){
			break;
		}
	}
};

void TCompetition::_simulateTrajectories(TPopSizeTrajectory & preIntroduction, TPopSizeTrajectories & postIntroduction, TRandomGenerator & RandomGenerator){
	_logfile->startIndent("Simulating trajectories:");

	//simulate with one species
	_logfile->listFlush("Simulating species 1 alone for T_1 = ", _duration[0]," ...");

	_simulateOneSpecies(preIntroduction, _duration[0], _initialSizes[0], _growthRates[0], _growthRateChange[0], _competitionMatrix[0], RandomGenerator);
	_logfile->done();

	//simulate with second species
	_logfile->listFlush("Simulating both species for T_2 = ",_duration[1], " ...");
	std::vector<double> growthRates = { _growthRates[0] * exp(_growthRateChange[0] * _duration[0]), _growthRates[1] };
	std::vector<uint32_t> N0 = {preIntroduction[_duration[0]], _initialSizes[1]};
	_simulateTwoSpecies(postIntroduction, _duration[1], N0, growthRates, _growthRateChange, _competitionMatrix, RandomGenerator);
	_logfile->done();

	//write to file
	std::string filename = _outName + "simulatedTrajectories.txt.gz";
	_logfile->listFlush("Writing trajectories to '" + filename + "' ...");
	TOutputFile out(filename, {"time", "N1", "N2"});
	uint32_t t = 0;
	for(; t<_duration[0]; ++t){
		out << t << preIntroduction[t] << "0" << std::endl;
	}
	for(uint32_t i=0; i<_duration[1]; ++i, ++t){
		out << t << postIntroduction[0][i] << postIntroduction[1][i] << std::endl;
	}
	_logfile->done();
	_logfile->endIndent();
};

void TCompetition::_calculatePopSizeDistributionOneSpecies(TPopSizeProbabilityDistSequence & probDist, const uint32_t duration, const uint32_t N0, const double growthRate, const double growthRateChange, const double competition){
	_logfile->listFlushTime("Calculating expected population Size Distribution without invader ...");

	probDist.reset(duration, N0);
	TPopSizeProbabilityDistKolmogorov p(N0);

	//now calculate
	for(uint32_t t=0; t<duration; ++t){
		for(uint32_t s=0; s<_stepsPerUnit; ++s){
			//get relevant growth rate
			double realTime = t + s * _deltaT;
			double growth = growthRate * exp(growthRateChange * realTime);

			p.kolmogorovStepOneSpecies(growth, competition, _deltaT);
		}

		//save p
		probDist[t+1] = p;
	}
	_logfile->doneTime();
};

void TCompetition::_calculateR(TPopSizeTrajectory & postIntroduction, TPopSizeProbabilityDistSequence & probDist, std::vector<double> & R){
	_logfile->listFlushTime("Calculating R ...");

	R.resize(postIntroduction.size());
	for(size_t i=0; i<postIntroduction.size(); ++i){
		R[i] = probDist[i].calculateR(postIntroduction[i]);
	}

	_logfile->doneTime();
};

void TCompetition::_calculateRho(TPopSizeTrajectory & postIntroduction, const TPopSizeProbabilityDistSequence & probDist, const double growthRate, const double growthRateChange, const double competition, std::vector<double> & rho){
	//prepare
	TPopSizeProbabilityDistKolmogorov p;
	uint32_t windowSize = _rhoWindow * _stepsPerUnit;
	uint32_t lastWindowStart = postIntroduction.size() - _rhoWindow;
	rho.resize(lastWindowStart);

	//report
	TProgressWithMessage prog(lastWindowStart, "Calculating rho ...", _logfile);

	//loop over windows
	for(uint32_t wStart = 0; wStart < lastWindowStart; ++wStart){
		//check if species died out
		if(postIntroduction[wStart + _rhoWindow] == 0){
			rho.resize(wStart);
			break;
		}

		//prepare
		rho[wStart] = 0.0;
		double obsRatio = postIntroduction[wStart + _rhoWindow] / (double) postIntroduction[wStart];

		//loop over relevant N at beginning of window
		for(uint32_t N0 = probDist.at(wStart).minN(); N0 <= probDist.at(wStart).maxN(); ++N0){
			//get transition probabilities
			p.reset(N0);
			for(uint32_t t=0; t<windowSize; ++t){
				//get relevant growth rate
				double realTime = _duration[0] + t * _deltaT;
				double growth = growthRate * exp(growthRateChange * realTime);

				p.kolmogorovStepOneSpecies(growth, competition, _deltaT);
			}

			//add to rho
			double tmp = obsRatio * N0 * probDist.at(wStart)[N0];
			//double tmp = N0 * probDist.at(wStart)[N0];
			for(uint32_t N = p.minN(); N <= p.maxN(); ++N){
				rho[wStart] += tmp / (double) N * p[N];
			}
		}

		//report progress
		prog.next();
	}
}

void TCompetition::_calculateRRho(TPopSizeTrajectory & preIntroduction, TPopSizeTrajectories & postIntroduction){
	_logfile->startIndent("Calculating R and rho:");

	//1) get pop size probability dist for first species in absence of second
	//-----------------------------------------------------------------------
	TPopSizeProbabilityDistSequence probDist;
	_calculatePopSizeDistributionOneSpecies(probDist, _duration[1], postIntroduction[0][0], _growthRates[0] * exp(_growthRateChange[0] * _duration[0]), _growthRateChange[0], _competitionMatrix[0]);

	//write quantiles
	std::string filename = _outName + "expectedPopulationSizeQuantilesNoInvasion.txt.gz";
	_logfile->listFlush("Writing quantiles of the expected population sizes to '" + filename + "' ...");
	probDist.writeQuantiles(filename, { 0.005, 0.025, 0.05, 0.25, 0.5, 0.75, 0.95, 0.975, 0.995}, _duration[0]);
	_logfile->done();

	//calculate R
	std::vector<double> R;
	_calculateR(postIntroduction[0], probDist, R);

	//calculate rho
	std::vector<double> rho;
	_calculateRho(postIntroduction[0], probDist,  _growthRates[0] * exp(_growthRateChange[0] * _duration[0]), _growthRateChange[0], _competitionMatrix[0], rho);

	//write
	filename = _outName + "R_rho.txt.gz";
	_logfile->listFlush("Writing R and rho to '" + filename + "' ...");
	TOutputFile out(filename, {"time", "R", "rho"});

	uint32_t t=0;
	for(; t<_rhoWindow; ++t){
		out << _duration[0] + t << R[t] << "NA" <<std::endl;
	}
	for(; t<rho.size(); ++t){
		out << _duration[0] + t << R[t] << rho[t] <<std::endl;
	}
	for(; t<R.size(); ++t){
		out << _duration[0] + t << R[t] << "NA" <<std::endl;
	}
	out.close();
	_logfile->done();

	_logfile->endIndent();
};

void TCompetition::simulate(TRandomGenerator & RandomGenerator){
	//simulate trajectories
	TPopSizeTrajectory preIntroduction;
	TPopSizeTrajectories postIntroduction;
	_simulateTrajectories(preIntroduction, postIntroduction, RandomGenerator);


	//calculate R and rho
	_calculateRRho(preIntroduction, postIntroduction);
};

