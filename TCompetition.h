/*
 * TCompetition.h
 *
 *  Created on: Oct 19, 2020
 *      Author: phaentu
 */

#include "TTask.h"
#include "TLog.h"
#include <algorithm>
#include <vector>

#ifndef TCOMPETITION_H_
#define TCOMPETITION_H_


//----------------------------------
// TPopSizeTrajectory
//----------------------------------
class TPopSizeTrajectory{
private:
	std::vector<uint32_t> _N;

public:
	TPopSizeTrajectory(){};
	TPopSizeTrajectory(const uint32_t duration, const uint32_t N0);
	void reset(const uint32_t duration, const uint32_t N0);

	uint32_t& operator[](size_t t){ return _N[t]; };
	size_t size(){ return _N.size(); };
};

class TPopSizeTrajectories{
private:
	std::vector<TPopSizeTrajectory> _Ns;

public:
	TPopSizeTrajectories(){};
	TPopSizeTrajectories(const uint32_t duration, const std::vector<uint32_t> N0);
	void reset(const uint32_t duration, const std::vector<uint32_t> N0);
	TPopSizeTrajectory& operator[](size_t species){ return _Ns[species]; };
};

//----------------------------------
// TPopSizeProbabilityDist
//----------------------------------
#define EXPANDSIZE 20
#define MINPROBCONSIDERED 1E-40

class TPopSizeProbabilityDist{
protected:
	std::vector<double> _probs; //used in Kolmogorov step
	uint32_t _minNStorage, _maxNStorage;
	uint32_t _minN, _maxN; //range of non-zero entries

	virtual void _expand();

public:
	TPopSizeProbabilityDist();
	TPopSizeProbabilityDist(const uint32_t initialN);
	virtual void reset(const uint32_t initialN);
	virtual ~TPopSizeProbabilityDist() = default;

	const double& operator[](size_t N) const { return _probs[N - _minN]; };
	uint32_t minN() const { return _minN; };
	uint32_t maxN() const { return _maxN; };
	std::vector<double>::const_iterator begin() const { return _probs.begin() + (_minN - _minNStorage); };
	std::vector<double>::const_iterator end() const { return _probs.begin() + (_maxN - _minNStorage) + 1; };

	void writeQuantiles(TOutputFile & out, const std::vector<double> & quantiles) const;
	double calculateR(const uint32_t obsN) const;
	void print() const;
};

class TPopSizeProbabilityDistKolmogorov:public TPopSizeProbabilityDist{
private:
	std::vector<double> _derivatives; //used in Kolmogorov step

	void _resetDerivatives();
	void _expand();

public:
	TPopSizeProbabilityDistKolmogorov();
	TPopSizeProbabilityDistKolmogorov(const uint32_t initialN);
	void reset(const uint32_t initialN);

	void kolmogorovStepOneSpecies(const double growthRate, const double competition, const double deltaT);
};

class TPopSizeProbabilityDistSequence{
private:
	std::vector<TPopSizeProbabilityDist> _dists;

public:
	TPopSizeProbabilityDistSequence(){};
	TPopSizeProbabilityDistSequence(const uint32_t duration, const uint32_t N0);
	void reset(const uint32_t duration, const uint32_t N0);

	TPopSizeProbabilityDist& operator[](size_t t){ return _dists[t]; };
	const TPopSizeProbabilityDist& at(size_t t) const { return _dists[t]; };
	void writeQuantiles(const std::string filename, const std::vector<double> & quantiles, const uint32_t addToTime) const;
};

//----------------------------------
// TPopSizeTransitionProbabilities
//----------------------------------
class TPopsizeJointStateProbabilities{
private:
	uint32_t N0;
	uint32_t _firstN;
	std::vector<double> _probs;

public:
	TPopsizeJointStateProbabilities();
	TPopsizeJointStateProbabilities(TPopSizeProbabilityDist & p);
	void reset(TPopSizeProbabilityDist & p);
};

class TPopSizeTransitionProbabilityMatrix{
private:
	std::vector<TPopsizeJointStateProbabilities> _transProbs;
	uint32_t _minN, _maxN;

public:
	TPopSizeTransitionProbabilityMatrix();
	void fill();
};

//----------------------------------
// TCompetition
//----------------------------------
class TCompetition{
private:
	TLog* _logfile;

	//competition model
	uint8_t _numSpecies;
	std::vector<uint32_t> _initialSizes;
	std::vector<double> _growthRates;
	std::vector<double> _growthRateChange;
	std::vector<double> _competitionMatrix;

	//simulation parameters
	std::vector<uint32_t> _duration;
	uint32_t _stepsPerUnit;
	double _deltaT;
	uint32_t _rhoWindow;
	std::string _outName;

	void _simulateOneSpecies(TPopSizeTrajectory & N, const uint32_t duration, const uint32_t N0, const double growthRate, const double growthRateChange, const double competition, TRandomGenerator & RandomGenerator);
	void _simulateTwoSpecies(TPopSizeTrajectories & N, const uint32_t duration, std::vector<uint32_t> & N0, const std::vector<double> growthRates, const std::vector<double> & growthRateChange, const std::vector<double> competition, TRandomGenerator & RandomGenerator);
	void _simulateTrajectories(TPopSizeTrajectory & preIntroduction, TPopSizeTrajectories & postIntroduction, TRandomGenerator & RandomGenerator);

	void _calculatePopSizeDistributionOneSpecies(TPopSizeProbabilityDistSequence & probDist, const uint32_t duration, const uint32_t N0, const double growthRate, const double growthRateChange, const double competition);
	void _calculateR(TPopSizeTrajectory & postIntroduction, TPopSizeProbabilityDistSequence & probDist, std::vector<double> & R);
	void _calculateRho(TPopSizeTrajectory & postIntroduction, const TPopSizeProbabilityDistSequence & probDist, const double growthRate, const double growthRateChange, const double competition, std::vector<double> & rho);
	void _calculateRRho(TPopSizeTrajectory & preIntroduction, TPopSizeTrajectories & postIntroduction);

public:
	TCompetition(TParameters & Parameters, TLog* Logfile);
	void simulate(TRandomGenerator & RandomGenerator);

};


//--------------------------------------
// Tasks
//--------------------------------------
class TTask_simulateCompetition:public TTask{
public:
	TTask_simulateCompetition(){ _explanation = "Printing pileup from BAM file"; };

	void run(TParameters & Parameters, TLog* Logfile){
		TCompetition competition(Parameters, Logfile);
		competition.simulate(*_randomGenerator);
	};

};



#endif /* TCOMPETITION_H_ */
