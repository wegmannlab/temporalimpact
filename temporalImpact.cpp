/*
 * estimHet.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: wegmannd
 */

#include "TMain.h"
#include "TCompetition.h"


void addTaks(TMain & main){
    // Use main.addRegularTask to add a regular task (shown in list of available tasks)

	//BAM
	main.addRegularTask("simulateCompetition", new TTask_simulateCompetition());
};

void addTests(TMain & main){
    // Use testing.addTest to add a single test

    // Use testing.addTestSuite to add a test suite (= a combination of tests)
    // main.addTestSuite("exampleSuite", {"example", "empty"});

};

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	TMain main("temporalImpact", "0.1", "https://bitbucket.org/wegmannlab/atlas", "daniel.wegmann@unifr.ch");


	//add existing tasks
	addTaks(main);

	//now run program
	return main.run(argc, argv);
};

